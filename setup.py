from setuptools import setup, find_packages
import sys
from os import path

if sys.version_info[0] == 2:
    sys.exit("Sorry, Python 2 is not supported")


this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name = 'micat',
      version = '0.0.1',
      description = 'Micat is for Microscopy Calibration and Testing using USAF resolution targets.',
      long_description = long_description,
      long_description_content_type='text/markdown',
      author = 'Richard Bowman and Julian Stirling',
      author_email = 'r.w.bowman@bath.ac.uk',
      packages = find_packages(),

      keywords = ['Microscopy','Calibration','Resolution'],
      zip_safe = True,
      url = 'https://gitlab.com/bath_open_instrumentation_group/micat',
      classifiers = [
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3.6'
          ],
      install_requires=['argparse','pyyaml>=5.1','numpy','scikit-image','opencv-python'],
      #Note: scikit-image needs scipy. scipy wont install on 3.8.
      python_requires=">=3.6,!=3.8",
      entry_points = {'console_scripts': ['micat = micat.__main__:main']},
      )

