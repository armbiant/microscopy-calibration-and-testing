#!/usr/bin/env python
import micat
import argparse
import os

def main():
    parser = argparse.ArgumentParser(description='Micat Microscopy Calibration and Testing', formatter_class=argparse.RawTextHelpFormatter)
    subparsers = parser.add_subparsers(help='Available commands listed below', metavar="<command>", dest='command')
    parser_usafcal = subparsers.add_parser('usafcal', help='Analyse an image of a USAF target')
    parser_usafcal.add_argument('imagefiles', type=str,nargs='+',
                    help='the image file to analyse')
    parser_usafcal.add_argument('--verbose', '-v', action='count')
    parser_usafcal.add_argument("--outdir","-O", help="Directory for saved data")
    parser_usafcal.add_argument("--group","-g",type=int,default=7, help="Set smallest group found, see PDF output! (default: 7)")
    parser_usafcal.add_argument("--element","-e",type=int,default=6, help="Set smallest element found, see PDF output! (default: 6)")
    parser_usafcal.add_argument("--largest","-L",action='count', help="Makes --group and --element to refer to largest not smallest")
    parser_help = subparsers.add_parser('help', help="Run 'help <command>' for detailed help")
    parser_help.add_argument('h_command', metavar='<command>',nargs='?', type=str, help = "Command to show help for")
    args = parser.parse_args()

    if args.command == 'usafcal':        
        
        assert args.group>0 and args.group<=7, 'Error: group must be an integer 1-7'
        assert args.group>0 and args.element<=6, 'Error: element must be an integer 1-6'
        
        if args.verbose is None:
            verbose = False
        else:
            verbose=True
            
        if args.largest is None:
            largest = False
        else:
            largest=True
        
        for filepath in args.imagefiles:
            if verbose:
                print('Analysing %s'%filepath)

            directory,filename = os.path.split(filepath)
            basefile,ext = os.path.splitext(filename)
            
            parameters = micat.usafcal.analyse_file(filepath,g=args.group,e=args.element,smallest=not largest,verbose=verbose)

            if args.outdir is not None:
                directory = args.outdir

            if verbose:
                print('Exporting results...')
            micat.usafcal.export_results(parameters,os.path.join(directory,basefile+"_USAF_calibration"))
            if verbose:
                print('Done!')

    
        
    
    elif args.command == 'help':
        if args.h_command is None:
            parser.print_help()
        elif args.h_command == 'usafcal':
            print(f"""\n`build` will build the documentation in the current folder,
make sure the current folder is a valid gitbuilding project.\n""")
            print(parser_usafcal.format_help())
        else:
            print(f'Invalid  micat command {args.h_command}\n\n')
            parser.print_help()
    else:
        print(f'Invalid micat command {args.command}')
        return None

if __name__ == '__main__':
    main()
    